# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import timedelta, date
from sql.aggregate import Sum
from sql.functions import DateTrunc, ToChar
from trytond.model import fields, Workflow, ModelView
from trytond.wizard import Wizard, StateTransition
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from decimal import Decimal
from .exceptions import (
    UserUnauthorizedError, ForwardEmailError, PurchaseQuotationError)
from trytond.i18n import gettext
import base64
from io import BytesIO
from trytond.modules.dash.dash import DashAppBase
import pandas as pd
import re

PRIORITY = [
    ('', ''),
    ('urgent', 'Urgent'),
    ('high', 'High'),
    ('low', 'Low')
]


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    priority = fields.Selection(PRIORITY, 'Priority')
    confirm_user = fields.Many2One('purchase.configuration.user_authorized',
        'Confirm User',
        states={'readonly': Eval('state') != 'draft'},
        domain=[('active', '=', True)]
    )
    confirmation_date = fields.Char('Confirmation Date',
        states={'readonly': True})
    confirmation_signature = fields.Function(fields.Binary('Signed Image'),
        'get_confirmation_signature')
    approval_user = fields.Many2One('purchase.configuration.user_authorized',
        'Approval User',
        states={'readonly': Eval('state') != 'draft'},
        domain=[('active', '=', True)]
    )
    approval_date = fields.Char('Approval Date', states={'readonly': True})
    approval_signature = fields.Function(fields.Binary('Approval Signature'),
        'get_approval_signature')

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls.state.selection.append(
            ('approved', 'Approved')
        )
        cls._transitions |= set((
            ('quotation', 'approved'),
            ('approved', 'confirmed'),
        ))
        cls._buttons.update({
            'approval': {
                'invisible': Eval('state') != 'quotation',
            },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approval(cls, purchases):
        config = cls.get_config()
        for purchase in purchases:
            if purchase.approval_user:
                authorized = purchase.check_is_authorized(
                    purchase.untaxed_amount, config)
                if not authorized:
                    continue
                _date = purchase.company.time_now()
                cls.write([purchase], {'approval_date': _date})

    @classmethod
    def get_config(cls):
        Config = Pool().get('purchase.configuration')
        config, = Config.search([], limit=1)
        return config

    def get_signature(self, name):
        """ This method is used in Purchase Report only"""
        signature = getattr(self, name)
        if signature:
            buffered = BytesIO(signature)
            img_str = buffered.getvalue()
            return [img_str, 'image/png']

    def get_confirmation_signature(self, name=None):
        User = Pool().get('res.user')
        if self.confirmation_date and self.confirm_user:
            user = User(self.confirm_user.user.id)
            return user.sign_image

    def get_approval_signature(self, name=None):
        User = Pool().get('res.user')
        if self.approval_date and self.approval_user:
            user = User(self.approval_user.user.id)
            return user.sign_image

    @classmethod
    def quote(cls, purchases):
        super(Purchase, cls).quote(purchases)
        for purchase in purchases:
            purchase.send_quotation_emails()

    def send_quotation_emails(self):
        pool = Pool()
        config = pool.get('purchase.configuration')(1)
        Template = pool.get('email.template')
        if not self.confirm_user or not self.confirm_user.user.email:
            return
        Template.send(
            config.template_email_confirm, self, self.confirm_user.user.email)

    def check_for_quotation(self):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        config = Configuration(1)
        if config.require_authorized and not self.confirm_user:
            raise PurchaseQuotationError(
                gettext('dash_purchase.msg_require_approved_by')
            )

    @classmethod
    def draft(cls, purchases):
        super(Purchase, cls).draft(purchases)
        cls.write(purchases, {'confirmation_date': None, 'approval_date': None, })

    @classmethod
    def confirm(cls, purchases):
        config = cls.get_config()
        for purchase in purchases:
            if purchase.confirm_user:
                authorized = purchase.check_is_authorized(purchase.untaxed_amount, config)
                if not authorized:
                    continue

                _date = purchase.company.time_now()
                cls.write([purchase], {'confirmation_date': _date})
            super(Purchase, cls).confirm([purchase])

    @classmethod
    def dash_get_taxes(cls, args, ctx=None):
        pool = Pool()
        Product = pool.get('product.product')
        product = Product(args['id'])
        rate = Decimal('0.0')
        value = Decimal('0.0')
        positive_taxes = [t for t in product.template.supplier_taxes_used \
            if (t.type == 'percentage' and t.rate > 0)]
        if positive_taxes:
            for tax in positive_taxes:
                if tax.rate and tax.type == 'percentage':
                    rate += tax.rate
                elif tax.rate:
                    value += tax.rate

        res = {
            'percentage': rate,
            'fixed': value
        }
        return res

    @classmethod
    def dash_quotation(cls, args, ctx=None):
        pool = Pool()
        Party = pool.get('party.party')
        Product = pool.get('product.product')
        Company = pool.get('company.company')
        Shop = pool.get('sale.shop')
        record = args.copy()
        try:
            party_id = record['party']['id']
        except Exception:
            party_id = record['party']
        party = Party(party_id)
        address = Party.address_get(party)
        context = Transaction().context
        company = Company(context['company'])
        currency = company.currency
        shop = Shop(context['shop'])
        lines = args['lines']
        nested_values = None
        for line in lines:
            if line and line[0] == 'create':
                nested_values = line[1]
        if nested_values:
            lines = nested_values
        for line in lines:
            if line.get('id'):
                del line['id']
            if line.get('amount'):
                del line['amount']
            product = Product(line['product'])
            unit = product.template.purchase_uom.id
            line['type'] = 'line'
            line['unit'] = unit
            line['unit_price'] = Decimal(line['unit_price']).quantize(Decimal(1) / 10 ** 4)
            line['description'] = product.name
            taxes = list(product.account_category.supplier_taxes_used)
            taxes_ids = [t.id for t in taxes]
            line['taxes'] = [('add', taxes_ids)]

        try:
            payment_term_id = record['payment_term']['id']
        except Exception:
            payment_term_id = record['payment_term']
        purchase_to_create = {
            'company': company.id,
            'party': party.id,
            'invoice_address': address,
            'description': record.get('description', ''),
            'payment_term': payment_term_id,
            'purchase_date': record['purchase_date'],
            'delivery_date': record['purchase_date'],
            'reference': record.get('reference', ''),
            'currency': currency.id,
            'warehouse': shop.warehouse.id,
            'state': 'draft',
            'comment': record.get('comment', ''),
            'lines': [('create', lines)],
        }
        res = {}
        record = args.copy()
        # remove try, if request generate error this return raise error and prevent reset form 
        # try:
        purchase, = cls.create([purchase_to_create])
        # except Exception as e:
        #     res.update({
        #         'record': record,
        #         'msg': str(e),
        #         'status': 'error',
        #         'type': 'error',
        #         'open_modal': True,
        #     })
        #     return res

        # try:
        cls.quote([purchase])
        # except Exception as e:
        #     res.update({
        #         'record': record,
        #         'msg': str(e),
        #         'status': 'error',
        #         'type': 'error',
        #         'open_modal': True,
        #     })
        #     return res

        record.update({
            'id': purchase.id,
            'state': purchase.state,
            'number': purchase.number,
            'total_amount': purchase.total_amount,
        })
        res.update({
            'record': record,
            'msg': f'Compra Nº {purchase.number}',
            'status': 'success',
            'type': 'success',
            'open_modal': True,
        })
        return res

    def check_is_authorized(self, amount, config):
        pool = Pool()
        UserAuthorized = pool.get(
            'purchase.configuration.user_authorized'
        )
        user_id = Transaction().user
        if config and (self.confirm_user or self.approval_user):
            authorized_user = UserAuthorized.search([
                ('configuration', '=', config.id),
                ('user', '=', user_id),
            ])
            if not authorized_user:
                if not Transaction().context.get('raise_exception'):
                    raise UserUnauthorizedError(
                        gettext('dash_purchase.msg_user_not_authorized'))
                else:
                    return False
            else:
                _authorized_user = authorized_user[0]
            if not _authorized_user.limit_amount_confirm \
                or int(amount) > _authorized_user.limit_amount_confirm:
                if not Transaction().context.get('raise_exception'):
                    raise UserUnauthorizedError(gettext(
                        'dash_purchase.msg_purchase_amount_not_authorized',
                        amount=str(amount)))
                else:
                    return False
            else:
                return True

    @classmethod
    def copy(cls, purchases, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['confirm_user'] = None
        default['confirmation_date'] = None
        default['approval_user'] = None
        default['approval_date'] = None
        return super(Purchase, cls).copy(purchases, default=default)


class ForwardPurchaseMail(Wizard):
    'Forward Purchase Mail'
    __name__ = 'purchase.forward_mail'
    start_state = 'forward_mail'
    forward_mail = StateTransition()

    @classmethod
    def __setup__(cls):
        super(ForwardPurchaseMail, cls).__setup__()

    def transition_forward_mail(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        ids = Transaction().context['active_ids']
        if ids:
            purchase = Purchase(ids[0])
            if purchase.state == 'quotation':
                purchase.send_quotation_emails()
            else:
                raise ForwardEmailError(gettext(
                    'dash_purchase.msg_state_purchase_draft',
                    s=purchase.number, state=purchase.state))
        return 'end'


class AppPurchase(DashAppBase):
    'App Purchase'
    __name__ = 'dash.app.purchase'


class AppPurchaseApproval(DashAppBase):
    'App Purchase Approval'
    __name__ = 'dash.app.purchase_approval'
    # project = fields.Boolean('Project')


class AppPurchaseRequisition(DashAppBase):
    'App Purchase Requisition'
    __name__ = 'dash.app.purchase_requisition'

    @classmethod
    def get_history(cls, args):
        # return dict {header: data as list [], records: list[list[values]]}
        pool = Pool()
        Product = pool.get('product.product')
        option = args.get('option')
        product_id = args.get('product')
        invoice_line = pool.get('account.invoice.line').__table__()
        invoice = pool.get('account.invoice').__table__()
        party = pool.get('party.party').__table__()
        configuration = pool.get('purchase.configuration')(1)
        try:
            limit_days = configuration.requisition_history_sale
        except:
            limit_days = 90

        where = invoice.state.in_(['posted', 'paid'])
        where &= invoice_line.product == product_id

        cursor = Transaction().connection.cursor()
        today = date.today()
        header = []
        records = []
        if option == 'sale_history':
            time_delta = today - timedelta(days=limit_days)

            where &= invoice.invoice_date >= str(time_delta)
            where &= invoice.invoice_date <= str(today)
            where &= invoice.type == 'out'
            columns = [
                ToChar(DateTrunc('month', invoice.invoice_date), 'YYYY-MM').as_('month'),
                Sum(invoice_line.quantity).as_('quantity'),
                ToChar(Sum(invoice_line.quantity*invoice_line.unit_price),
                    'fm999G999D99').as_('amount'),
            ]
            query = invoice_line.join(invoice, condition=invoice_line.invoice == invoice.id
                ).select(*columns,
                where=where,
                group_by=[DateTrunc('month', invoice.invoice_date)],
                order_by=[DateTrunc('month', invoice.invoice_date).asc]
                )
            cursor.execute(*query)
            result = cursor.fetchall()
            header = ['MES', 'CANTIDAD', 'VALOR']
            records = result
        elif option == 'purchases':
            where &= invoice.type == 'in'

            columns = [
                party.name.as_('supplier'),
                invoice.invoice_date.as_('invoice_date'),
                invoice_line.quantity.as_('quantity'),
                ToChar(invoice_line.unit_price, 'fm999G999G999D99').as_('unit_price'),
                ToChar((invoice_line.quantity * invoice_line.unit_price),'fm999G999G999D99').as_('amount'),
            ]
            query = invoice_line.join(invoice, condition=invoice_line.invoice == invoice.id
                ).join(party, condition=invoice.party == party.id
                ).select(*columns,
                where=where,
                order_by=[invoice.invoice_date.desc],
                limit=4)
            cursor.execute(*query)
            result = cursor.fetchall()
            header = ['PROVEEDOR', 'FECHA', 'CANTIDAD', 'VALOR', 'TOTAL']
            records = result
        elif option == 'current_stock':
            header = ['BODEGA', 'CANTIDAD']
            res = Product.get_stock_by_locations({'code': Product(product_id).code})
            records = res
        return {
            'header': header,
            'records': records
        }

    @classmethod
    def create_purchase(cls, args):
        pool = Pool()
        PurchaseRequest = pool.get('purchase.request')
        dom = [
            ('party', '=', args['supplier']),
            ('quantity', '>', 0),
        ]
        if args.get('brand'):
            dom.append(('product.template.brand', '=', args['brand']))

        if args.get('warehouse'):
            dom.append(('warehouse', '=', args['warehouse']))

        if args.get('requests'):
            dom.append(('id', 'in', args['requests']))
        requests = PurchaseRequest.search(dom)
        if not requests:
            return {
                'status': 'error',
                'msg': 'No se han encontrado requesiciones para crear compra'}
        CreatePurchase = pool.get('purchase.request.create_purchase', type='wizard')
        session_id, _, _ = CreatePurchase.create()
        CreatePurchase.records = requests
        CreatePurchase.execute(session_id, {}, 'start')
        CreatePurchase.delete(session_id)
        return {'status': 'success', 'msg': 'Compra creada exitosamente...!'}

    @classmethod
    def process_file(cls, args):
        files = args.get('file_upload')
        pool = Pool()
        PurchaseRequest = pool.get('purchase.request')
        Product = pool.get('product.product')
        if files:
            name = files[0]
            ext = name.split('.')[1]
            file_string = files[1]
            f_string = base64.b64decode(file_string).decode('utf8')
            file_base64 = re.search(r'base64,(.*)', f_string).group(1)
            bytes_ = BytesIO(base64.b64decode(file_base64))
            if ext in ('xls', 'xlsx'):
                engine = 'openpyxl'
            elif ext in ('ods', 'fods'):
                engine = 'odf'
            df_file = pd.io.excel.read_excel(bytes_, engine=engine, header=0)
            df_file.columns = df_file.columns.str.lower()
            references = list(df_file.loc[:, 'referencia'])
            brands = list(set(df_file.loc[:, 'marca']))
            requests = PurchaseRequest.search_read([
                ('product.reference', 'in', references),
                ('product.brand.name', 'in', brands)],
                fields_names=['id', 'product.reference'])
            products_found = Product.search_read([
                ('reference', 'in', references),
                ('brand.name', 'in', brands),
            ], fields_names=['id', 'reference'])
            reference_found = [r['reference'] for r in products_found]
            request_ids = [r['id'] for r in requests]
            df_not_request = df_file[~df_file.referencia.isin(reference_found)]
            df_not_request = df_not_request.astype(dtype={"valor": "float64"})
            df_not_request.columns = df_not_request.columns.str.upper()
            not_request = df_not_request.to_dict(orient="split")
            return {'requests': request_ids, 'not_request': not_request, 'status': 'success'}
