# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class DashApp(metaclass=PoolMeta):
    __name__ = 'dash.app'

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend([
            'dash.app.purchase',
            'dash.app.purchase_approval',
            'dash.app.purchase_requisition'
        ])
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend([
            ('purchase', 'Purchase'),
            ('purchase_approval', 'Purchase Approval'),
            ('purchase_requisition', 'Purchase Requisition'),
        ])
        return options
